﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HomeTask1.Models;

namespace HomeTask1.Controllers
{ 
    public class SortingController : Controller
    {
        private SortingDBContext db = new SortingDBContext();

        public ViewResult Index()
        {
            return View(db.Sort.ToList());
        }

        public ActionResult Delete(int id)
        {
            Sorting sorting = db.Sort.Find(id);
            db.Sort.Remove(sorting);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public ActionResult OK(int id = 0)
        {
            string vector = Request.Form["vector"].ToString();
            if (vector.Length == 0)
                ModelState.AddModelError("Vector", "Vector is empty");
            string choice = Request.Form["select"].ToString();
            
            if (ModelState.IsValid)
            {
                Sorting newItem = new SimpleSort();
                switch (choice)
                {
                    case "Shell":
                        newItem.sortMethod = new Shell();
                        break;
                    case "Insertion":
                        newItem.sortMethod = new Insertion();
                        break;
                    case "Quick":
                        newItem.sortMethod = new Quick();
                        break;
                }
                newItem.Unsorted = vector;
                try
                {
                    newItem.performSort();
                    db.Sort.Add(newItem);
                    db.SaveChanges();
                }
                catch(Exception)
                {

                }
            }
        return RedirectToAction("Index");
    }
    }
}