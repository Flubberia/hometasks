﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace HomeTask1.Models
{
    //Pattern Strategy

    //interface of variable aspect
    public interface Sortable
    {
        string Sort(int[] vector);
    }

    //Different implementations of behavior
    public class Shell : Sortable
    {
       public string Sort(int[] vector)
        {
            int gap = vector.Length / 2;
            while (gap > 0)
            {
                for (int i = 0; i < vector.Length - gap; i++)
                {
                    int j = i + gap;
                    int tmp = vector[j];
                    while (j >= gap && tmp.CompareTo(vector[j - gap]) == -1)
                    {
                        vector[j] = vector[j - gap];
                        j -= gap;
                    }
                    vector[j] = tmp;
                }
                if (gap == 2)
                {
                    gap = 1;
                }
                else
                {
                    gap = (int)(gap / 2.2);
                }
            }
            
            return String.Join(",",vector);
        }
    }
    public class Insertion : Sortable
    {
        public string Sort(int[] vector)
        {
            for (int i = 0; i < vector.Length - 1; i++)
            {
                int j = i + 1;
                int tmp = vector[j];
                while (j > 0 && tmp.CompareTo(vector[j - 1]) == -1)
                {
                    vector[j] = vector[j - 1];
                    j--;
                }
                vector[j] = tmp;

            }
            return String.Join(",", vector);
        }
    }
    public class Quick : Sortable
    {   
        private void Swap(int[] vector, int left, int right)
        {
            int tmp = vector[left];
            vector[left] = vector[right];
            vector[right] = tmp;
        }
        private void QuickSort(int[] vector, int left, int right)
        {
            if (left < right)
            {
                int bound = left;
                for (int i = left + 1; i < right; i++)
                {
                    if (vector[i].CompareTo(vector[left]) == -1)
                    {
                        Swap(vector, i, ++bound);
                    }
                }
                Swap(vector, left, bound);
                QuickSort(vector, left, bound);
                QuickSort(vector, bound + 1, right);
            }
        }
        public string Sort(int[] vector)
        {
            QuickSort(vector, 0, vector.Length);
            return String.Join(",", vector);
        }
    }

    //Main superclass for all subclasses that have some ordered and unorderes sequnces 
    public abstract class Sorting
    {   
        public Sortable sortMethod{ get; set; }
        public int ID { get; set; }
        public string Sorted{ get; set; }
        public string Unsorted { get; set; }

        public void performSort() 
        {
            try
            {
                List<int> toSort = new List<int>();
                foreach (var i in Unsorted.Trim(' ').Split(','))
                {
                    toSort.Add(int.Parse(i));
                }
                Sorted = sortMethod.Sort(toSort.ToArray());
            }
            catch(Exception e)
            {
                throw (e);
            }
        }
    }

    //Subtype of specific variety
    public class SimpleSort : Sorting{}

    public class SortingDBContext : DbContext
    {
        public DbSet<Sorting> Sort { get; set; }
    }
}